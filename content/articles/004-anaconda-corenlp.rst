=============================================
Stanford dependency parser, NLTK and Anaconda
=============================================

:date: 2015-08-21 21:03
:tags: deployment, parsing

.. contents::
    :depth: 1

.. class:: uk-alert uk-alert-warning

.. warning::

    As for June 27 2016, this post is outdated. I suggest to check `this pull request`__ to NLTK that supports CoreNLP HTTP API.

    __ https://github.com/nltk/nltk/pull/1249

For a while I've been looking for a `dependency parser`_ that is easy to
deploy, configure and use. Today I came across the `Stanford CoreNLP 3.5.2
<http://nlp.stanford.edu/software/corenlp.shtml>`_, which supports `dependency
parsing <http://nlp.stanford.edu/software/nndep.shtml>`_. After a day of playing
with it, I have a simple setup.

.. _`dependency parser`: https://en.wikipedia.org/wiki/Syntax#Dependency_grammar

The main difficulty for me is that I use Python, while most parsers are written
in Java. NLTK provides an interface to a parser, but for every call it starts a
Java virtual machine. This is fine if you want to parse a sentence or two, but
has a too large overhead if you want to parse many chunks of text.

A way of solving this is to expose the parsing API via HTTP, so a parser is
started only once. The best implementation I've found is `Wordseer's stanford-corenlp-python
fork <https://github.com/Wordseer/stanford-corenlp- python>`_.

Installation
============

Stanford tools require Java 1.8, I suppose it's installed:

.. code-block:: bash

    $ java -version
    java version "1.8.0_60"
    Java(TM) SE Runtime Environment (build 1.8.0_60-b27)
    Java HotSpot(TM) 64-Bit Server VM (build 25.60-b23, mixed mode)

Get CoreNLP:

.. code-block:: bash

    $ wget http://nlp.stanford.edu/software/stanford-corenlp-full-2015-04-20.zip
    $ unzip http://nlp.stanford.edu/software/stanford-corenlp-full-2015-04-20.zip

For the Python distribution, I use `Anaconda
<https://store.continuum.io/cshop/anaconda/>`_ (check this `installation
instructions
<http://eecs.io/python-environment-for-scientific-computing.html>`_), a fresh
environment and a `custom`__ stanford-corenlp-python package.

__ https://github.com/Wordseer/stanford-corenlp-python/pull/10/files

.. code-block:: bash

    $ ~/miniconda3/bin/conda create -n parser python=2.7 -y
    $ . ~/miniconda3/bin/activate parser
    (parser) $ conda install -c https://conda.binstar.org/dimazest stanford-corenlp-python -y




Usage
=====

Starting a server is easy:

.. code-block:: bash

    (parser) $ corenlp -S stanford-corenlp-full-2015-04-20
    Serving on http://127.0.0.1:8080

Calling the server (from another terminal window):

.. code-block:: bash

    $ . ~/miniconda3/bin/activate parser
    (parser) $ python

with some sample code

.. code-block:: python

    >>> import json, jsonrpclib
    >>> from pprint import pprint
    >>>
    >>> server = jsonrpclib.Server("http://localhost:8080")
    >>>
    >>> pprint(json.loads(server.parse('John loves Mary.')))  # doctest: +SKIP
    {u'sentences': [{u'dependencies': [[u'root', u'ROOT', u'0', u'loves', u'2'],
                                       [u'nsubj',
                                        u'loves',
                                        u'2',
                                        u'John',
                                        u'1'],
                                       [u'dobj', u'loves', u'2', u'Mary', u'3'],
                                       [u'punct', u'loves', u'2', u'.', u'4']],
                     u'parsetree': [],
                     u'text': u'John loves Mary.',
                     u'words': [[u'John',
                                 {u'CharacterOffsetBegin': u'0',
                                  u'CharacterOffsetEnd': u'4',
                                  u'Lemma': u'John',
                                  u'PartOfSpeech': u'NNP'}],
                                [u'loves',
                                 {u'CharacterOffsetBegin': u'5',
                                  u'CharacterOffsetEnd': u'10',
                                  u'Lemma': u'love',
                                  u'PartOfSpeech': u'VBZ'}],
                                [u'Mary',
                                 {u'CharacterOffsetBegin': u'11',
                                  u'CharacterOffsetEnd': u'15',
                                  u'Lemma': u'Mary',
                                  u'PartOfSpeech': u'NNP'}],
                                [u'.',
                                 {u'CharacterOffsetBegin': u'15',
                                  u'CharacterOffsetEnd': u'16',
                                  u'Lemma': u'.',
                                  u'PartOfSpeech': u'.'}]]}]}

Integration with NLTK
=====================

I'll use NLTK with Python 3 (just because I can) and jsonrpclib:

.. code-block:: bash

    $ ~/miniconda3/bin/conda create -n nltk python=3.4 nltk -y
    $ . ~/miniconda3/bin/activate nltk
    (nltk) $ conda install -c https://conda.binstar.org/dimazest jsonrpclib -y

``DependencyGraph`` is a powerful datastructure to store information about the
sentence. However, the output from the parser needs to be transformed before
being passed to ``DependencyGraph``.

.. code-block:: python

    >>> import jsonrpclib, json
    >>> from nltk.parse import DependencyGraph
    >>>
    >>> server = jsonrpclib.Server("http://localhost:8080")
    >>> parses = json.loads(
    ...    server.parse(
    ...       'John loves Mary. '
    ...       'I saw a man with a telescope. '
    ...       'Ballmer has been vocal in the past warning that Linux is a threat to Microsoft.'
    ...    )
    ... )['sentences']
    >>>
    >>> def transform(sentence):
    ...     for rel, _, head, word, n in sentence['dependencies']:
    ...         n = int(n)
    ...
    ...         word_info = sentence['words'][n - 1][1]
    ...         tag = word_info['PartOfSpeech']
    ...         lemma = word_info['Lemma']
    ...         if rel == 'root':
    ...             # NLTK expects that the root relation is labelled as ROOT!
    ...             rel = 'ROOT'
    ...
    ...         # Hack: Return values we don't know as '_'.
    ...         #       Also, consider tag and ctag to be equal.
    ...         # n is used to sort words as they appear in the sentence.
    ...         yield n, '_', word, lemma, tag, tag, '_', head, rel, '_', '_'
    ...
    >>> dgs = [
    ...     DependencyGraph(
    ...         ' '.join(items)  # NLTK expects an iterable of strings...
    ...         for n, *items in sorted(transform(parse))
    ...     )
    ...     for parse in parses
    ... ]
    >>>
    >>> # Play around with the information we've got.
    >>>
    >>> pprint(list(dgs[0].triples()))
    [(('loves', 'VBZ'), 'nsubj', ('John', 'NNP')),
     (('loves', 'VBZ'), 'dobj', ('Mary', 'NNP')),
     (('loves', 'VBZ'), 'punct', ('.', '.'))]
    >>>
    >>> print(dgs[1].tree())
    (saw I (man a (with (telescope a))) .)
    >>>
    >>> print(dgs[2].to_conll(4))  # doctest: +NORMALIZE_WHITESPACE
    Ballmer     NNP     4       nsubj
    has         VBZ     4       aux
    been        VBN     4       cop
    vocal       JJ      0       ROOT
    in          IN      4       prep
    the         DT      8       det
    past        JJ      8       amod
    warning     NN      5       pobj
    that        WDT     13      dobj
    Linux       NNP     13      nsubj
    is          VBZ     13      cop
    a           DT      13      det
    threat      NN      8       rcmod
    to          TO      13      prep
    Microsoft   NNP     14      pobj
    .           .       4       punct
    <BLANKLINE>


With some tricks it's possible to feed custom data to DependencyGragh, even
though we had to adopt to its expectations: an iterable of sorted string; thus
there is a ``sort()`` call and the items are joined to a string.

``DependencyGraph`` needs to be improved so these tricks are not necessary.
Ideally, just passing an iterable with information for every word and a custom
function for ``cell_extractor``. Have a look to `NLTK source`__ for more
details.

__ https://github.com/nltk/nltk/blob/c9e836a5b302d8a477d24c6e1fa9620440a24edc/nltk/parse/dependencygraph.py#L225

Thanks to jsonrpclib, I've called code written in Python 2 from Python 3. It
should be possible to run the Stanford parser wrapper on PyPy_ for performance
improvements and hide a cluster of them behind of a load balancer in case you
want to parse `Wikipedia`_ quickly.

.. _PyPy: http://pypy.org/
.. _Wikipedia: https://dumps.wikimedia.org/enwiki/latest/
