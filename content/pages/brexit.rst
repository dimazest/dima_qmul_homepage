=================================================
`#brexit`_ tweets collected in May and June 2016
=================================================

:url: brexit
:save_as: brexit/index.html
:date: 2016-06-24 22:39
:slug: brexit

.. contents::
    :depth: 2

.. image:: {filename}/static/brexit/brexit_timeline.svg
    :width: 100%

Files
=====

* `brexit_timeline.csv <{filename}/static/brexit/brexit_timeline.csv>`_
  The number of tweets per hour:

  .. code-block:: bash

    head brexit_timeline.csv
    2016-05-06-11 2044
    2016-05-06-12 2241
    2016-05-06-13 2494
    2016-05-06-14 2092
    2016-05-06-15 2126
    2016-05-06-16 2245
    2016-05-06-17 2006
    2016-05-06-18 1675
    2016-05-06-19 1708
    2016-05-06-20 1563

* `brexit_tweets_ids.csv.gz <{filename}/static/brexit/brexit_tweets_ids.csv.gz>`_
  The tweet ids:

  .. code-block:: bash

    zcat brexit_tweets_ids.csv.gz | head  # Try gzcat if zcat doesn't work.
    728541290133127168
    728541295304712192
    728541297045344257
    728541299649941504
    728541301138984964
    728541301013155840
    728541314497712132
    728541317094084608
    728541326673907713
    728541326673887234

Collecting the data
-------------------

You can get the data using twarc_ and poultry_:

.. code-block:: bash

    # Set up the credentials
    export CONSUMER_KEY=...
    export CONSUMER_SECRET=...
    export ACCESS_TOKEN=...
    export ACCESS_TOKEN_SECRET=...

    mkdir t

    # Hydrate the tweets using twarc and group them with poultry by day.
    time gzcat brexit_tweets_ids.csv.gz | twarc.py --hydrate - | poultry group -t 't/%Y-%m-%d.gz'
    t/2016-05-06.gz
    t/2016-05-07.gz
    ...


    # Half a million tweets are collected in about 4 hours.

Hackathon
=========

.. raw:: html

    <blockquote class="twitter-tweet" data-lang="en-gb"><p lang="en" dir="ltr">To be clear, I’m looking for people and companies who is interested in organising a hackathon dedicated to the analysis of <a href="https://twitter.com/hashtag/brexit?src=hash">#brexit</a> tweets</p>&mdash; dimazest (@dimazest) <a href="https://twitter.com/dimazest/status/747792620060020736">28 June 2016</a></blockquote>

There is `an idea to organise a hackathon`__. The plan is to gather people with
different backgrounds, think what the tweets might be useful for and implement
the ideas.

If you have a suggestion, please comment below.

__ https://twitter.com/dimazest/status/747792620060020736

Ideas
=====

.. raw:: html

    <blockquote class="twitter-tweet" data-conversation="none" data-lang="en-gb"><p lang="en" dir="ltr"><a href="https://twitter.com/dimazest">@dimazest</a> no. of capitalized words and <a href="https://twitter.com/hashtag/leave?src=hash">#leave</a> vs <a href="https://twitter.com/hashtag/stay?src=hash">#stay</a></p>&mdash; Liling Tan (@alvations) <a href="https://twitter.com/alvations/status/746386684757979137">24 June 2016</a></blockquote>
    <blockquote class="twitter-tweet" data-conversation="none" data-lang="en-gb"><p lang="en" dir="ltr"><a href="https://twitter.com/dimazest">@dimazest</a> Be interested to go back to an old thought about disrupting social media filter bubbles - how to improve deliberative dialogue?</p>&mdash; Shauna (@shaunaconcannon) <a href="https://twitter.com/shaunaconcannon/status/746675531471536129">25 June 2016</a></blockquote>
    <blockquote class="twitter-tweet" data-conversation="none" data-lang="en-gb"><p lang="en" dir="ltr"><a href="https://twitter.com/dimazest">@dimazest</a> have you looked at clustering of people changing through time? Eg two echo chambers for each side?</p>&mdash; Dan Frost (@danfrost) <a href="https://twitter.com/danfrost/status/748202952913985536">29 June 2016</a></blockquote>
    <blockquote class="twitter-tweet" data-conversation="none" data-lang="en-gb"><p lang="en" dir="ltr"><a href="https://twitter.com/dimazest">@dimazest</a> <a href="https://twitter.com/j_w_baker">@j_w_baker</a> <a href="https://twitter.com/ernestopriego">@ernestopriego</a> sure. Topic modelling?</p>&mdash; Neuroliviⓐ (@o_guest) <a href="https://twitter.com/o_guest/status/748133470304636928">29 June 2016</a></blockquote>
    <blockquote class="twitter-tweet" data-lang="en-gb"><p lang="en" dir="ltr">Emotions for <a href="https://twitter.com/hashtag/brexit?src=hash">#brexit</a>: First <a href="https://twitter.com/hashtag/fear?src=hash">#fear</a>, then <a href="https://twitter.com/hashtag/sadness?src=hash">#sadness</a>, then <a href="https://twitter.com/hashtag/anger?src=hash">#anger</a> and <a href="https://twitter.com/hashtag/disgust?src=hash">#disgust</a>. No <a href="https://twitter.com/hashtag/joy?src=hash">#joy</a> <a href="https://t.co/5sxxcpq0nt">pic.twitter.com/5sxxcpq0nt</a></p>&mdash; Roman Klinger (@roman_klinger) <a href="https://twitter.com/roman_klinger/status/748260997241180160">29 June 2016</a></blockquote>
    <blockquote class="twitter-tweet" data-lang="en-gb"><p lang="en" dir="ltr"><a href="https://twitter.com/hashtag/brexit?src=hash">#brexit</a> discussion: from UK and US in May to EU, US and Australia later on and worldwide during and after. <a href="https://t.co/2nltqSZmsY">pic.twitter.com/2nltqSZmsY</a></p>&mdash; dimazest (@dimazest) <a href="https://twitter.com/dimazest/status/749283479519715328">2 July 2016</a></blockquote>
    <blockquote class="twitter-tweet" data-cards="hidden" data-lang="en-gb"><p lang="en" dir="ltr">EU Referendum Analysis: Media, Voters and the Campaign - Early reflections from leading UK academics  <a href="https://t.co/Rx8Mw3pJWj">https://t.co/Rx8Mw3pJWj</a></p>&mdash; thor magnusson (@thormagnusson) <a href="https://twitter.com/thormagnusson/status/750003681244901376">4 July 2016</a></blockquote>

    <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

Heatmap
=======

.. raw:: html

    <iframe width="100%" height="520" frameborder="0" src="https://dimazest.cartodb.com/viz/557fdb50-3afa-11e6-bbe7-0ea31932ec1d/embed_map" allowfullscreen webkitallowfullscreen mozallowfullscreen oallowfullscreen msallowfullscreen></iframe>

Useful materials
================

* `Mining Twitter Data with Python <http://www.kdnuggets.com/2016/06/mining-twitter-data-python-part-1.html>`_
* `Personality Traits and Echo Chambers on Facebook <http://arxiv.org/abs/1606.04721>`_
* `#BrexitSyllabus <https://twitter.com/search?q=%23BrexitSyllabus>`_
* `A Syllabus for Understanding Brexit <https://docs.google.com/document/d/1RDP55BtP6fRZbDkFC9MGeTwAuch9zFylKit4R9BT5T4/edit>`_
* `Should it stay or should it go: News outlets scramble to cover Britain’s decision to exit the European Union <http://www.niemanlab.org/2016/06/should-it-stay-or-should-it-go-news-outlets-scramble-to-cover-britains-decision-to-exit-the-european-union/>`_
* `Political Polarization on Twitter <http://www.aaai.org/ocs/index.php/ICWSM/ICWSM11/paper/viewFile/2847/3275.pdf>`_
* `A Preliminary Study of Tweet Summarization using Information Extraction <http://aclweb.org/anthology/W/W13/W13-1103.pdf>`_
* `Capturing and Preserving the EU Referendum Debate (Brexit) <http://blogs.bl.uk/webarchive/2016/06/capturing-and-preserving-the-eu-referendum-debate-brexit.html>`_

Software
--------

* twarc_ and twarc-report_
* poultry_

.. _twarc: https://github.com/edsu/twarc/
.. _twarc-report: https://github.com/pbinkley/twarc-report

Attribution
===========

The tweets were collected with Poultry_ using the infrastructure of
the `School of Electronic Engineering and Computer Science`__ at
`Queen Mary University of London`__.

__ http://eecs.qmul.ac.uk/
__ http://qmul.ac.uk/

Poultry_ was created as a part of my master thesis where it was used to collect
tweets about music festivals in Europe and the London Olympics. Later it resulted in
a `paper`__ presented at the `WWW 13`_ workshop RAMSS_ in Rio.

__ http://www2013.org/companion/p795.pdf

.. _`#brexit`: https://twitter.com/search?q=%23brexit
.. _poultry: http://poultry.readthedocs.io
.. _`WWW 13`: http://www2013.org/
.. _`RAMSS`: http://www2013.org/program/ramss-real-time-analysis-and-mining-of-social-streams/
