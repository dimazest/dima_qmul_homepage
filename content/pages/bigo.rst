===================================
Big-O: A broad view on word vectors
===================================

:URL: bigo
:save_as: bigo/index.html
:date: 2015-08-18 11:26:44
:summary: Big-O talk
:slug: bigo

..

:code: `IPython notebook <{filename}/static/bigo15.ipynb>`_
:nbviewer: `A rendered version`__
:slides: `bigo.pdf <{filename}/static/bigo.pdf>`_
:toy space: `space_frame_eecs14.h5 <{filename}/static/eecs_open14/space_frame_eecs14.h5>`_ co-occurrence counts for just a few words
:23k space: `bigo_matrix.h5.gz <{filename}/static/data/bigo_matrix.h5.gz>`_ co-occurrence counts for 23000 words

 __ http://nbviewer.ipython.org/urls/www.eecs.qmul.ac.uk/~dm303/static/bigo15.ipynb


References
==========

word2vec
--------

.. publications:: content/static/references.bib
    :sort: none
    :entries:
        mikolov2013linguistic
        mikolov2013distributed
        mikolov2013efficient
        goldberg2014word2vec
        DBLP:journals/corr/Rong14
        levy2014linguistic
        DBLP:journals/corr/AroraLLMR15

Background
----------

.. publications:: content/static/references.bib
    :sort: none
    :entries:
        harris1954distributional
        firth1957lingtheory
        J90-1003
        Dagan:1993:CWS:981574.981596

word2vec and other models
-------------------------

.. publications:: content/static/references.bib
    :sort: none
    :entries:
        NIPS2014_5477
        TACL570
        suzuki-nagata:2015:ACL-IJCNLP
        pennington2014glove

Related work
------------
.. publications:: content/static/references.bib
    :sort: none
    :entries:
        mitchell-lapata:2008:ACLMain
        Baroni2010nouns
        Turney:2010:FMV:1861751.1861756
        DBLP:journals/corr/abs-1003-4394
