=========
EMLP 2014
=========

:URL: emnlp14.html
:save_as: emnlp14.html
:date: 2014-11-05 14:44
:summary: Evaluating Neural Word Representations in Tensor-Based Compositional Settings
:slug: emnlp14

..

:paper: Evaluating Neural Word Representations in Tensor-Based Compositional Settings
:authors: Dmitrijs Milajevs, Dimitri Kartsaklis, Mehrnoosh Sadrzadeh, Matthew Purver
:conference: http://emnlp2014.org/
:download: `D14-1079.pdf <http://www.aclweb.org/anthology/D/D14/D14-1079.pdf>`__

::

  @InProceedings{milajevs-EtAl:2014:EMNLP2014,
    author    = {Milajevs, Dmitrijs  and  Kartsaklis, Dimitri  and  Sadrzadeh, Mehrnoosh  and  Purver, Matthew},
    title     = {Evaluating Neural Word Representations in Tensor-Based Compositional Settings},
    booktitle = {Proceedings of the 2014 Conference on Empirical Methods in Natural Language Processing (EMNLP)},
    month     = {October},
    year      = {2014},
    address   = {Doha, Qatar},
    publisher = {Association for Computational Linguistics},
    pages     = {708--719},
    url       = {http://www.aclweb.org/anthology/D14-1079}
  }

Experiment data
===============

* `BNC corpus <http://www.natcorp.ox.ac.uk/>`_
* `ukWaC <http://wacky.sslmit.unibo.it/>`_
* `word2vec <https://code.google.com/p/word2vec/>`_
* `Disambiguation dataset <http://www.cs.ox.ac.uk/activities/compdistmeaning/GS2011data.txt>`_
* `Sentence similarity dataset <http://www.cs.ox.ac.uk/activities/compdistmeaning/emnlp2013_turk.txt>`_
* `MS paraphrase <http://research.microsoft.com/en-us/downloads/607d14d9-20cd-47e3-85bc-a2f65cd28042>`_
* `Switchboard <http://compprag.christopherpotts.net/swda.html>`_

Software
========

`fowler.corpora <http://fowlercorpora.readthedocs.org/en/latest/>`_ was developed
to run the experiments. By the time of writing, the package is still in the
early development stage. If you are interested in using it contact
d.milajevs@qmul.ac.uk or dimazest@gmail.com.

`Numpy <http://www.numpy.org/>`_, `scipy <http://www.scipy.org/>`_
and `scikit-learn <http://scikit-learn.org/stable/>`_ performed computations.
`pandas <http://pandas.pydata.org/>`_ did IO and data management.
