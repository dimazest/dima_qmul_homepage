============
Reading List
============

:date: 2015-06-16 13:20
:slug: reading-list

.. contents::

Similarity
==========

.. publications:: content/static/references.bib
    :sort: none
    :entries:
        Tversky1977
        Verheyen2016
        Goldstone1994obtainingsimilarity
        Griffiths2007
        Goldstone1994similarity
        1986-13502-00119860101
        Dagan:1993:CWS:981574.981596
        Agirre:2009:SSR:1620754.1620758
        turney2012domain
        hill2014simlex
        banjade2015lemontea
        doi:10.1080/01690969108406936
        zweig2014explicit
        Townsend1971confusion
        Rosenberg1975sorting
        borg2012multidimensional
        hahn1997concepts
        Nosofsky199194
        Hahn20031
        Hahn1998understanding
        Roth1983346
        Sadler1993
        WCS:WCS1282
        FILLENBAUM197454
        HENLEY1969176

Phrasal
-------

.. publications:: content/static/references.bib
    :sort: none
    :entries:
        rubinstein-EtAl:2015:ACL-IJCNLP
        gershman2015phrase


Meaning representation
======================

.. publications:: content/static/references.bib
    :entries:
        Turney:2010:FMV:1861751.1861756
        Dowty1980

Matrix factorization
--------------------

.. publications:: content/static/references.bib
    :entries:
        pennington2014glove
        DBLP:journals/corr/YogatamaFDS14
        2015arXiv150602761J
        lebret2015rehabilitation
        DBLP:journals/corr/LebretL13
        TACL570

Neural nets
-----------

.. publications:: content/static/references.bib
    :entries:
        2015arXiv150605514S
        NIPS2013_5165
        bordes2012joint
        vulic-moens:2015:ACL-IJCNLP
        xu2015effectiveness
        2015arXiv150607650X
        LeCun2015deep
        DBLP:journals/corr/Rong14
        DBLP:journals/corr/Schmidhuber14
        DBLP:journals/corr/KirosZSZTUF15
        DBLP:journals/corr/TraskGR15
        2015arXiv150705523L
        2015arXiv150704808S
        iyyer-EtAl:2014:EMNLP2014
        socher-EtAl:2013:EMNLP
        chen-EtAl:2015:EMNLP1

Random projections
------------------

.. publications:: content/static/references.bib
    :entries:
        DBLP:journals/corr/DemskiURK14

Manifold learning
-----------------

.. publications:: content/static/references.bib
    :entries:
        Croce:2010:MLS:1870516.1870518
        Jurgens:2010:CNS:1870516.1870517
        DBLP:journals/corr/CzarneckiJT15

Cluster based ambeddings, spectral learning, HMM
------------------------------------------------

.. publications:: content/static/references.bib
    :entries:
        dhillon2015eigenwords

Raw co-occurrence
-----------------

.. publications:: content/static/references.bib
    :entries:
        Dagan:1993:CWS:981574.981596

Vector space transformation
---------------------------

.. publications:: content/static/references.bib
    :entries:
        faruqui-EtAl:2015:ACL-IJCNLP
        2015arXiv150701127R
        osterlund-odling-sahlgren:2015:EMNLP
        BullinariaLevy2012

Evaluation
----------

.. publications:: content/static/references.bib
    :entries:
        lapesa2014large
        santus-EtAl:2015:LDL
        kiela-clark:2014:CVSC
        2002:PSC:503104.503110
        Rubenstein:1965:CCS:365628.365657
        kiela-hill-clark:2015:EMNLP
        2015arXiv151102024S

Other
-----

.. publications:: content/static/references.bib
    :entries:
        DBLP:journals/corr/BeltagyRCEM15
        2015arXiv150605230F
        DBLP:journals/corr/AroraLLMR15
        2015arXiv150601070L
        rubinstein-EtAl:2015:ACL-IJCNLP
        schwartz-reichart-rappoport:2015:Conll
        faruqui-EtAl:2015:NAACL-HLT
        pilehvar2015senses
        zhang-EtAl:2015:ACL-IJCNLP1
        suzuki-nagata:2015:ACL-IJCNLP
        J90-1003
        2015arXiv151000259H

Quantification
==============

.. publications:: content/static/references.bib
    :entries:
        lowetowards
        church1999inverse
        Aizawa200345
        ferret2010testing
        doi:10.1108/00220410410560582
        bouma2009normalized
        pecina2008machine
        Grefenstette:2011:ESC:2145432.2145580

Composition
===========

.. publications:: content/static/references.bib
    :entries:
        mitchell2010composition
        mitchell-lapata:2008:ACLMain
        Baroni2010nouns
        pham-et-al:2013:IWCS2013:TFDS
        2015arXiv150605703L
        wieting2015paraphrase
        blacoe2012comparison
        basili2015compositional

Tensor based
------------

.. publications:: content/static/references.bib
    :entries:
        DBLP:journals/corr/PolajnarRC14
        kim2015neural
        polajnar-fagarasan-clark:2014:EMNLP2014
        hashimoto-EtAl:2014:EMNLP2014
        DBLP:journals/corr/IrsoyC14
        hashimoto-tsuruoka:2015:CVSC
        AAAI159597
        fried-polajnar-clark:2015:ACL-IJCNLP
        DBLP:journals/corr/abs-1003-4394
        kartsadrqpl2014

IR
==

.. publications:: content/static/references.bib
    :entries:
        vulic2015monolingual
        Lioma:2015:NTD:2766462.2767717
        Zheng:2015:LRT:2766462.2767700

Evaluation
----------

.. publications:: content/static/references.bib
    :entries:
        DBLP:conf/clef/2001
        schnabel-EtAl:2015:EMNLP

Topic models
============

.. publications:: content/static/references.bib
    :entries:
        das2015gaussian
        griffiths2007topics
        griffiths2005integrating
        boyd-graber2009syntactic
        wallach2009rethinking
        Doyle:2009:ABT:1553374.1553410
        DBLP:journals/corr/NiuD15
        2015arXiv150704798R
        das-zaheer-dyer:2015:ACL-IJCNLP

Discourse
=========

.. publications:: content/static/references.bib
    :entries:
        DBLP:journals/corr/JiE14a
        DBLP:journals/corr/JiE14
        ge-xu:2015:ACL-IJCNLP
        kalchbrenner-blunsom:2013:CVSC
        polajnar-rimell-clark:2015:LSDSem
        bernardi-EtAl:2015:LSDSem

Language modelling
==================

.. publications:: content/static/references.bib
    :entries:
        Brown:1992:CNG:176313.176316
        Mnih:2007:TNG:1273496.1273577
        morin2005hierarchical
        2015arXiv150701193M
        DBLP:journals/corr/abs-1103-0398
        mirowski-vlachos:2015:ACL-IJCNLP

.. TODO

    Relations
    =========
    .. publications:: content/static/references.bib
        :sort: date
        :entries:
            paccanaro2002learning
            paccanaro2000extracting

Matrix factorization
====================

.. publications:: content/static/references.bib
    :entries:
        10.1109/MC.2009.263
        Nati03weightedlow-rank
        2010arXiv1003.4944P

Neural nets
===========
.. publications:: content/static/references.bib
    :entries:
        2015arXiv150802788B

* `Inceptionism: Going Deeper into Neural Networks <http://googleresearch.blogspot.co.uk/2015/06/inceptionism-going-deeper-into-neural.html>`_
* `The Unreasonable Effectiveness of Recurrent Neural Networks <http://karpathy.github.io/2015/05/21/rnn-effectiveness/>`_
* `“Behold molten children”: Recurrent Neural Networks for Generating Even More Word of God <https://highnoongmt.wordpress.com/2015/05/26/behold-molten-children-recurrent-neural-networks-for-generating-even-more-word-of-god/>`_
* `The unreasonable effectiveness of Character-level Language Models (and why RNNs are still cool) <http://nbviewer.ipython.org/gist/yoavg/d76121dfde2618422139>`_
* `Critique of Paper by "Deep Learning Conspiracy" (Nature 521 p 436) <http://people.idsia.ch/~juergen/deep-learning-conspiracy.html>`_
* `What my deep model doesn't know... <http://mlg.eng.cam.ac.uk/yarin/blog_3d801aa532c1ce.html>`_
* `A Statistical View of Deep Learning <http://blog.shakirm.com/2015/07/a-statistical-view-of-deep-learning-retrospective/>`_
* `Why does Deep Learning work? <https://charlesmartin14.wordpress.com/2015/03/25/why-does-deep-learning-work/>`_
* `Understanding Convolutional Neural Networks for NLP <http://www.wildml.com/2015/11/understanding-convolutional-neural-networks-for-nlp/>`_

Parsing
=======
.. publications:: content/static/references.bib
    :entries:
        choi-tetreault-stent:2015:ACL-IJCNLP

Twitter
=======
.. publications:: content/static/references.bib
    :entries:
        lukasik-cohn-bontcheva:2015:ACL-IJCNLP
        2015arXiv150700955K

Tasks
=====

.. publications:: content/static/references.bib
    :entries:
        agirre-soroa:2007:SemEval-20071
        strapparava-mihalcea:2007:SemEval-2007
        litkowski-hargraves:2007:SemEval-2007
        navigli-litkowski-hargraves:2007:SemEval-2007
        mccarthy-navigli:2007:SemEval-2007
        mihalcea-sinha-mccarthy:2010:SemEval
        lefever-hoste:2010:SemEval
        pustejovsky-EtAl:2010:SemEval
        manandhar-EtAl:2010:SemEval
        specia-jauhar-mihalcea:2012:STARSEM-SEMEVAL
        jurgens-EtAl:2012:STARSEM-SEMEVAL
        agirre-EtAl:2012:STARSEM-SEMEVAL
        hendrickx-EtAl:2013:SemEval-2013
        agirre-EtAl:2013:*SEM1
        navigli-vannella:2013:SemEval-2013
        jurgens-klapaftis:2013:SemEval-2013
        korkontzelos-EtAl:2013:SemEval-2013
        marelli-EtAl:2014:SemEval
        jurgens-pilehvar-navigli:2014:SemEval
        mihalcea-chklovski-kilgarriff:2004:Senseval-3

Various
=======
.. publications:: content/static/references.bib
    :sort: none
    :entries:
        2015arXiv150602078K
        chrupala:2014:P14-2
        2015arXiv150606714S
        sandor2012connecting
        chen2015velda
        chang-EtAl:2014:EMNLP2014
        DBLP:journals/corr/WangGBTL15
        zhang-EtAl:2014:EMNLP20145
        DBLP:journals/corr/DenilDKBF14
        2015arXiv150601094G
        Perfors2011306
        2015arXiv150608909L
        2015arXiv150701529M
        Hirschberg17072015
        Tversky1977
        43146
        DBLP:journals/corr/YogatamaS15
        doi:10.1080/01621459.2012.734168
        ELX2014-022
        harris1954distributional
        DBLP:journals/corr/abs-1202-1056
        2015arXiv151000277G
        2015arXiv151102014K
        Gazdar:1996:PMN:242807.242813

* `How I got into linguistics, and what I got out of it <http://www.ling.upenn.edu/~wlabov/Papers/HowIgot.html>`_
* `ACL 2016 Tutorial: Understanding Short Texts <http://www.wangzhongyuan.com/tutorial/ACL2016/Understanding-Short-Texts/>`_
