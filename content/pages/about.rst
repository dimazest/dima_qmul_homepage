=================
Dmitrijs Milajevs
=================

:URL:
:save_as: index.html
:template: main
:date: 2013-12-13 19:24
:summary: The university page of Dmitrijs Milajevs at Queen Mary, University of London

I started my Ph.D. at Queen Mary in October 2013. My supervisors are `Dr.
Mehrnoosh Sadrzadeh`__ and `Dr. Matthew Purver`__.

__ http://www.eecs.qmul.ac.uk/people/view/33472/dr-mehrnoosh-sadrzadeh
__ http://www.eecs.qmul.ac.uk/~mpurver/


As a computational linguist I'm mostly interested in **compositional
distributional semantics** and its application to **dialog analysis**. As a
computer scientist I'm interested in **distributed systems**, automated
**testing** and **big data**. Finally, as programmer I'm interested in
**Python**. Check my `CV <{filename}/static/dmilajevs_cv.pdf>`__ for more
details.

Publications
============

The `bibliography file`__ and my `Google Scholar page`__.

__ {filename}/static/dmilajevs_publications.bib
__ https://scholar.google.co.uk/citations?user=CScje3kAAAAJ&hl=en

.. publications:: content/static/dmilajevs_publications.bib
    :sort: date

.. TODO: remove

  .. [EMNLP14] Dmitrijs Milajevs, Dimitri Kartsaklis, Mehrnoosh Sadrzadeh and Matthew
    Purver. `Evaluating Neural Word Representations in Tensor-Based Compositional
    Settings`__. EMNLP_, Doha, Qatar, October 2014. bib__ page__

    __ http://www.aclweb.org/anthology/D/D14/D14-1079.pdf
    .. _EMNLP: http://emnlp2014.org/
    __ {filename}/static/bib/milajevs-et-al14emnlp.bib
    __ {filename}/pages/emnlp14.rst

  .. [CVSC14] Dmitrijs Milajevs and Matthew Purver. `Investigating the Contribution of
    Distributional Semantic Information for Dialogue Act Classification`__. In
    Proceedings of the EACL__ Workshop on `Continuous Vector Space Models and
    their Compositionality (CVSC)`__, Gothenburg, April 2014. bib__ page__

    __ {filename}/static/papers/cvsc14/milajevs-purver14cvsc.pdf
    __ http://www.eacl2014.org/
    __ https://sites.google.com/site/cvscworkshop2014/
    __ {filename}/static/bib/milajevs-purver14cvsc.bib
    __ {filename}/pages/cvsc14.rst

Projects
========

Check out a tweet collection manager called poultry_ and
google-ngram-downloader_. I also contribute to NLTK_.

.. _google-ngram-downloader: https://pypi.python.org/pypi/google-ngram-downloader

.. _NLTK: http://www.nltk.org/

Activities
==========

2016
----

* March

  * `The Collaborations Workshop`__, Edinburgh, UK. During the hackathon we
    developed `guidelines for citing software`__. See the comic__!

    __ http://www.software.ac.uk/cw16
    __ http://mr-c.github.io/shouldacite/index.html
    __ https://cdn.rawgit.com/mr-c/shouldacite/master/shoudacite_comic.pdf

  * Participated in `#newsHACK 2016`__, London, UK. Our project was about `multilingual
    named entity linking in news`__.

    __ http://bbcnewslabs.co.uk/2016/03/17/newsHACK-language-hack/
    __ https://twitter.com/dimazest/status/710148104545312768

2015
----

* September

  * Participated in organizing a `Royal Institution Masterclass`__ about
    natural language processing.

    __ http://www.eecs.qmul.ac.uk/public-engagement/ri-masterclasses

* August

  * `A broad new on word vectors <{filename}/pages/bigo.rst>`_,
    a presentation at the `Big-O London meetup`__.

    __ http://www.meetup.com/big-o-london/events/224383139/

* April

  * Got involved in IWCS_ organization, London, UK.

  .. _IWCS : http://iwcs2015.github.io/

  * Organized an `academic hackathon at IWCS`__, London, UK.

    __ http://iwcs2015.github.io/hackathon.html

  * A poster presentation at `the Advances in Distributional Semantics Workshop`__, London, UK.

    __ https://sites.google.com/site/iwcs2015ads/

* March

  * A lighting talk about workflow management at `the Collaborations Workshop`__, Oxford, UK.

    __ http://www.software.ac.uk/cw15

  * Participated at the `Symposium and Hackathon in Social Media and
    Interaction`__, Emmanuel College, Cambridge University, UK. `Our team won
    the first prize`__ and goes to Mumbai!

    __ http://www.planthack.org/
    __ http://www.qmul.ac.uk/media/news/items/se/151554.html

2014
____

* October

  * A paper__ presentation__ at `EMNLP <http://emnlp2014.org/>`_, Doha, Qatar.

    __ http://emnlp2014.org/papers/pdf/EMNLP2014079.pdf
    __ https://www.youtube.com/watch?v=CUPpCmv8ijg

  * A presentation at a `DisCo Project`__ meeting University of Edinburgh, UK.

    __ https://sites.google.com/site/discoprojectuk/

* July

  * A lighting talk at `EuroPython <https://ep2014.europython.eu/en/>`_,
    Berlin, Germany.

* May

  * A talk about basic linguistics at `PyGrunn <http://www.pygrunn.org/>`_,
    Groningen, The Netherlands.

* April

  * A `paper`__ presentation on applying distributional semantics for dialog
    act tagging at `the 2nd Workshop on Continuous Vector Space Models and
    their Compositionality`__, Gothenburg, Sweden.

    __ http://www.eecs.qmul.ac.uk/~dm303/cvsc14.html
    __ https://sites.google.com/site/cvscworkshop2014/

  * A `poster`__ presentation at `EECS Research Showcase`__, London, UK.

    __ http://www.eecs.qmul.ac.uk/~dm303/eecs_open14.html
    __ http://www.eecs.qmul.ac.uk/research/research-showcase-2014

* March

  * `Collaborations Workshop`__ (CW14), Oxford, UK. CW is a very
    friendly gathering of people who are concerned about qualitative research
    software. `A related post`__ on how to setup a recomputable experiment.

    __ http://software.ac.uk/cw14
    __ http://qmcs.io/software-in-your-reproducible-research.html

* February

  * A lighting talk at `PyData London <http://pydata.org/ldn2014>`_ about `opster <http://opster.readthedocs.org/>`_ and `poultry <http://poultry.readthedocs.org/>`_.
