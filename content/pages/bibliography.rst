==============
 Bibliography
==============

:date: 2015-06-16 13:20
:save_as: bibliogrpahy.html
:url: bibliogrpahy.html

.. publications:: content/static/references.bib
    :sort: none-reversed
