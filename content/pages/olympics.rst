=======================================
Olympic tweets collected in summer 2016
=======================================

:url: olympics
:save_as: olympics/index.html
:date: 2016-08-05 11:08

.. contents::
    :depth: 2

.. image:: {filename}/static/olympics/olympics_timeline.svg
    :width: 100%

Files
=====

* `olympics_timeline.csv <{filename}/static/olympics/olympics_timeline.csv>`_ The number of tweets per hour.

* `olympics_tweets_ids.csv.gz <{filename}/static/olympics/olympics_tweets_ids.csv.gz>`_ The tweet ids:

Attribution
===========

The tweets were collected with Poultry_ using the infrastructure of
the `School of Electronic Engineering and Computer Science`__ at
`Queen Mary University of London`__.

__ http://eecs.qmul.ac.uk/
__ http://qmul.ac.uk/

Poultry_ was created as a part of my master thesis where it was used to collect
tweets about music festivals in Europe and the London Olympics. Later it resulted in
a `paper`__ presented at the `WWW 13`_ workshop RAMSS_ in Rio.

__ http://www2013.org/companion/p795.pdf

.. _`#brexit`: https://twitter.com/search?q=%23brexit
.. _poultry: http://poultry.readthedocs.io
.. _`WWW 13`: http://www2013.org/
.. _`RAMSS`: http://www2013.org/program/ramss-real-time-analysis-and-mining-of-social-streams/
