===========
NLP seminar
===========

:date: 2014-04-03 11:00
:slug: nlp-seminar

We usually meet on Tuesdays at 11:00 in the ITL Meeting room on the top floor. A
`bib file <{filename}/static/references.bib>`_ with the paper entries is
available in case you need to cite some paper.

.. contents::
    :depth: 1

Summer 2015
===========

Phrase meaning representation (Jun 23)
--------------------------------------

.. publications:: content/static/references.bib
     :sort: none
     :entries: Erk:2009:RWR:1596374.1596387


Machine translation (May 12)
----------------------------

.. publications:: content/static/references.bib
     :sort: none
     :entries: kay1997proper

Phrase similarity (May 19)
--------------------------

.. publications:: content/static/references.bib
     :sort: none
     :entries: gershman2015phrase


Topic models (Jun 2)
--------------------

.. publications:: content/static/references.bib
     :sort: none
     :entries: das2015gaussian

Word meaning representation (Jun 23)
------------------------------------

.. publications:: content/static/references.bib
     :sort: none
     :entries: Erk:2009:RWR:1596374.1596387

Spring 2015
===========

Similarity, association and relatedness (Jan 21)
------------------------------------------------

.. publications:: content/static/references.bib
     :sort: none
     :entries: hill2014simlex
               zeelenberg1999priming

Conceptual spaces (Jan 28)
--------------------------

.. publications:: content/static/references.bib
     :sort: none
     :entries: jager2014conceptual
               jager2007evolution
               jager2010natural

Meaning representation (Feb 3)
------------------------------

.. publications:: content/static/references.bib
     :sort: none
     :entries: DBLP:journals/corr/VilnisM14

Meaning representation II (Feb 10)
----------------------------------

.. publications:: content/static/references.bib
     :sort: none
     :entries: widdows2004geometric

Discourse compositionality (Feb 17)
-----------------------------------

.. publications:: content/static/references.bib
     :sort: none
     :entries: DBLP:journals/corr/DenilDKBF14

Language modelling (Feb 24)
---------------------------

.. publications:: content/static/references.bib
     :sort: none
     :entries: TOPS:TOPS12034

Meaning representation (Mar 3)
------------------------------

.. publications:: content/static/references.bib
     :sort: none
     :entries: DBLP:journals/corr/AroraLLMR15

Machine learning (Mar 10)
-------------------------

.. publications:: content/static/references.bib
   :sort: none
   :entries: liang2015bringing

Autumn 2014: Natural language modeling: from n-grams to vectors
===============================================================

There was a trend of going away from predefined categorical features, such as
context words in distributional semantics, n-grams in language modeling, and
bag-of-words models in information retrieval. Instead the features are `learned`
in some way. The goal of the reading seminar is to go trough this evolution on
an example of language modeling and word representation in a vector space.

Towards the end several papers are suggested per meeting. The reason is that
they share the same topic. We don't need to read all of them, but rather chose
the ones we like.


Introduction to n-gram models (Oct 6)
-------------------------------------

"Foundations of statistical natural language processing" chapter 6 up to section
6.2.3. In addition, we can have a look how parameters are estimated using
Maximum Likelihood Estimation in Bernoulli Experiment. This is the list of books
that might give the necessary background in language processing and statistics.
They are available in the library__.

__ http://www.library.qmul.ac.uk/

.. publications:: content/static/references.bib
    :sort: none
    :entries: manning1999foundations
              bishop2006pattern
              martin2000speech

An class-based language model (Oct 13)
--------------------------------------

Matthew leads a discussion on model validation. Stephen introduces the class based n-gram model of Brown et al.

.. publications:: content/static/references.bib
    :sort: none
    :entries: Brown:1992:CNG:176313.176316

A backing-off language model (Oct 20)
-------------------------------------

Focus on the problem of data sparsity: unseen n-grams are assigned zero
probabilities. Discuss smoothing techniques.

.. publications:: content/static/references.bib
    :sort: none
    :entries: kneser1995improved

A log-linear language model (Nov 03)
------------------------------------

Go trough a derivation of a `log-linear language model <http://www.cs.columbia.edu/~mcollins/loglinear.pdf>`_.

.. publications:: content/static/references.bib
    :sort: none
    :entries: Mnih:2007:TNG:1273496.1273577

A neural language model (Nov 10)
--------------------------------

Discuss how neural networks approach the problem of data sparsity. Basically,
they aim to solve the issue of unseen data in the following way. Suppose, the
trigram ``dogs chase cats`` is seen often in the training corpus. Also ``cats``
and ``kittens`` share a lot of context in the training corpus. Even if ``dogs
chase kittens`` is never seen in the corpus, the model will assign a
probability to it close to ``dogs chase cats`` **because** similar words are
expected to have similar feature vectors.

.. publications:: content/static/references.bib
    :sort: none
    :entries: bengio2006

Scaling up a neural language model
----------------------------------

It is expensive to train a neural model, because of normalisation in softmax.
Discuss possible solutions: hierarchical softmax and negative sampling.

.. publications:: content/static/references.bib
    :sort: none
    :entries: mnih2012fast
              NIPS2008_3583
              bengio2008adaptive
              Collobert:2008:UAN:1390156.1390177
              NIPS2013_5165
              DBLP:journals/corr/NepalY13

word2vec
--------

Mikolov et al. scaled up neural networks for dictionaries of million of words.

.. publications:: content/static/references.bib
    :sort: none
    :entries: mikolov2013linguistic
              mikolov2013distributed
              mikolov2013efficient
              goldberg2014word2vec

Deeper than the deep, or understanding word2vec
-----------------------------------------------

.. publications:: content/static/references.bib
    :sort: none
    :entries: levy2014linguistic
              pennington2014glove
              NIPS2014_5477

Related materials
-----------------

* http://cl.naist.jp/~kevinduh/notes/cwmt14tutorial.pdf
* http://cl.naist.jp/~kevinduh/a/deep2014/
* http://snippyhollow.github.io/blog/2014/08/09/so-you-wanna-try-deep-learning/

.. publications:: content/static/references.bib
    :sort: none
    :entries: DBLP:journals/corr/SzegedyZSBEGF13
              pennington2014glove
              NIPS2014_5477
              Luo:2014:SCM:2663792.2663793
