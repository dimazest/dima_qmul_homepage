========
Projects
========

:date: 2014-08-11 16:18
:summary: Project ideas
:slug: projects

..

.. contents::
    :depth: 1

Distributional semantics and morphology
=======================================

:status: draft
:meant for: postgraduate students

Distributional semantics is proven to be effective to model meaning of words and now is extended to capture meaning of
phrases and sentences. In a widely adopted setup, words are the smallest units of composition: ``love`` and ``loved``
both get assigned vectors.

The problem is that the source corpus has to be pretty large. For example, the `British National Corpus`_ (100 million
word collection) does not provide enough information to be useful for the dialog act tagging and MS Paraphrase tasks
[EMNLP14]_.

.. _British National Corpus: http://www.natcorp.ox.ac.uk/

To overcome data sparsity, one could build word vectors from `morpheme vectors`. Then, to come up with a vector
representation of ``loved``, one needs to compose together the vector of the stem ``love`` and the morpheme ``-ed``.
Have a look to [blunsom2014morphology]_ and [lazaridou2013morphology]_.

The task
--------

Check whether any research has been done about this problem. Think of an appropriate vector space for morphemes (is it
the same as the word vector space). What compositional operator should be used (would vector addition or pointwise
multiplication be sufficient, or a more complex composition is required). Implement a compositional method and evaluate
it on the Switchboard dialog act tagging task and MS Paraphrase corpus.

Requirements
------------

You will read a lot to get familiar with the field. Then you will extend an existing system written in Python.

Compositional document representation
=====================================

:status: draft
:meant for: postgraduate students

Compositional distributional semantics studies how a sentence meaning can be obtained from its parts by performing
operations on word vectors. However, it's not clear how to compose sentence vectors to get a vector representation of a
document.

The task
--------

Study the current ways of document representation (e.g. bag of word models). Implement simple compositional model based
on vector addition and pointwise multiplication.

You also will need to think of a task on which the method will be evaluated. It might be a document retrieval or
document similarity tasks.

Finally, a baseline has to be implemented and a comparison to the state-of-the-art performance reported.
[le2014distributed]_ might be a good starting point.

Requirements
------------

You will read a lot to get familiar with the field. Once you got an idea how such systems work, you should be able to
implement your own.

Dialog act tagging
==================

:status: draft
:meant for: undergraduate and postgraduate students

Computer systems that dial with dialogs (think of a chat bot or an automatic question answering system via phone) need
to know classify utterances. Answers, clarification questions, statements might trigger different system strategy.

One way of classifying an utterance is to build a vector representation from it's parts by composing word vectors and
then training a classifier. A K-nearest neighbor clustering algorithm using distributional word vectors is shown  to
outperform the same set up, but with a bag of words model [CVSC14]_.

Vector addition and pointwise multiplication are operations that don't respect word order, a phenomena that might be
important for distinguishing different utterance types. Categorical composition does respect word order, but requires
syntactic structure of an utterance.

The task
--------

1. Investigate whether `a more complex classier`__ improves tagging accuracy for the Switchboard corpus.

  __ http://scikit-learn.org/stable/modules/ensemble.html#forests-of-randomized-trees

2. Try categorical composition.


Requirements
------------

You should enjoy programming and don't be afraid of scientific papers. Some knowledge of machine learning is a plus. You
will code in Python.

Unified parsing API
===================

:status: draft
:meant for: undergraduate students

`Parsing`__ of text written in natural language is a challenging task both theoretically and practically. Basically
parsing assigns structure to a string, so later one could retrieve a relative clause, a subject of a sentence or
conclude that a sentence is grammatically correct. There are two major approaches: phrase-based and dependency-based.


__ http://en.wikipedia.org/wiki/Parsing#Human_languages

In phrase-based approaches, a sentence is seen as nested phrases. Consider `'John loves Mary.'`. A phrased-based parser
would derive the following phrases:

* ``John``: noun
* ``loves``: transitive verb
* ``Mary``: noun
* ``[loves Mary]``: verb phrase
* ``[John [loves Mary]]``: sentence

A dependency-based parser returns triples ``(head, relation, dependant)``:

* ``(loves, subject, John)``
* ``(loves, object, Mary)``

Real parsers that handle complex language operate on much more complex structures then the example above.

The task
--------

`Natural Language Toolkit <http://www.nltk.org/>`_ (NLTK) is a library that handles language related tasks. It is
possible to use NLTK to access state of the art parsers such as the Stanford Parser and the Malt parser.

Your task is to extend NLTK with interfaces to other parsers:

* `TurboParser <http://www.ark.cs.cmu.edu/TurboParser/>`_ preferably using Cython.
* `Stanford CoreNLP <http://nlp.stanford.edu/software/corenlp.shtml>`_ toolkit. Complete support of their multilingual
  pipeline [CoreNLP]_ possibly using an HTTP RESTful API (though `stanford-corenlp-python <https://github.com/dasmith/
  stanford-corenlp-python>`_ or `corenlp-python <https://pypi.python.org/pypi/corenlp-python>`_ might do the job).
* `C&C tools <http://svn.ask.it.usyd.edu.au/trac/candc>`_.
* `MaltParser <http://www.maltparser.org/>`_.
* `Illinois tools <http://cogcomp.cs.illinois.edu/page/software>`_.
* `Yara Parser <https://github.com/yahoo/YaraParser>`_.
* `Clearnlp <https://github.com/clir/clearnlp-guidelines>`_.
* `The LTH Constituent-to-Dependency Conversion Tool for Penn-style Treebanks <http://nlp.cs.lth.se/software/treebank-converter/>`_.

It must be possible to parse the following resources in a reasonable time:

* A recent `Wikipedia XML dump <https://dumps.wikimedia.org/enwiki/20140614/>`_ (about 2 billion tokens).

  * `linguatools <http://linguatools.org/tools/corpora/wikipedia-monolingual-corpora/>`_
  * `Wikipedia XML dump processing using gensim <http://radimrehurek.com/2013/12/performance-shootout-of-nearest-neighbours-contestants/>`_.
  * `WikiWoods <http://moin.delph-in.net/WikiWoods>`_.

  It might worth contacting the `Research and Data <https://www.mediawiki.org/wiki/Analytics/Research_and_Data>`__ and
  `Engineering <https://www.mediawiki.org/wiki/Analytics/Engineering>`__ teams to discuss with them whether it's
  possible to regularly parse the Wikipedia dumps using their infrastructure.

* The `ukWaC corpus <http://wacky.sslmit.unibo.it/doku.php?id=corpora>`_ (2 billion tokens).

  * `PukWaCDocumentIterator <https://github.com/fozziethebeat/S-Space/blob/master/src/main/java/edu/ucla/sspace/text/PukWaCDocumentIterator.java#L88>`__ read comments to get an idea what the lines are.
  * `WaCKyDependencyExtractor <https://github.com/fozziethebeat/S-Space/blob/master/src/main/java/edu/ucla/sspace/dependency/WaCKyDependencyExtractor.java#L54>`__ useful links to format description.
  * `CoNLLDependencyExtractor <https://github.com/fozziethebeat/S-Space/blob/master/src/main/java/edu/ucla/sspace/dependency/CoNLLDependencyExtractor.java#L154>`__ field description.
  * `NLTK Dependency Graphs <http://nbviewer.ipython.org/github/gmonce/nltk_parsing/blob/master/2.%20NLTK%20Dependency%20Graphs.ipynb>`__.

* The `COW corpus <http://hpsg.fu-berlin.de/cow/>`_.
* The `USENET corpus <http://www.psych.ualberta.ca/~westburylab/downloads/usenetcorpus.download.html>`_.
* The `Universal Dependencies <http://universaldependencies.github.io/docs/#language-en>`_ corpus.
* https://pypi.python.org/pypi/PyStanfordDependencies

Another useful resources:

* `A first approach to automatic text data cleaning
  <http://ianozsvald.com/2015/01/10/a-first-approach-to-automatic-text-data-
  cleaning/>`_ by Ian Ozsvald.
* `ftfy <http://ftfy.readthedocs.org/en/latest/>`_.
* http://barbar.cs.lth.se:8081/parse
* `Turbo parser web interface <http://demo.ark.cs.cmu.edu/parse?sentence=John%20loves%20Mary.>`_

Make sure that the NLTK documentation about parsing is complete, updated and comprehensive. Cover with unittests
existing and new code.

Requirements
------------

You should enjoy programming and don't be afraid of unfamiliar technologies. You will code in Python.

Parse Common Crawl
==================

The `Common Crawl data <http://commoncrawl.org/>`_ (5 billion webpages, 541TB of data is available at `Amazon Web
Services <https://aws.amazon.com/datasets/41740>`_) is a huge text corpus. Distributional methods benefit from large
resources as big datasets provide better statistics.

The task
--------

You will need to:

* Provide ngram counts.
* Provide co-occurrence statistics.
* Parse the corpora using a dependency parser.
* Provide dependency triples ``(head, relation, dependant)`` and how many times each triple was seen in the corpus.
* Provide counts for verbs occurring with their objects and subjects.

  For example: ``(love, subject, John) (love, object, Mary) 10``

* Provide the code and documentation so a new version of the dataset could be used to obtain the information above.
* Do qualitative evaluation: is the parser output reliable and useful for NLP task.

Requirements
------------

The amount of data will require a lot of engineering work. You need to be interested in developing a distributed system,
possibly based on Hadoop (since AWS provides infrastructure to execute Hadoop jobs). But the output of the task needs to
be accessible using Python (e.g. NLTK) or any other programming language.

Useful links
------------

* http://www.let.rug.nl/gosse/Wikipedia/enwiki.html
* http://www.let.rug.nl/basile/candcproxy/
* `Dictionary and co-occurrence extraction using gensim <http://radimrehurek.com/2014/12/making-sense-of-word2vec/>`__
* `nlpnet <https://github.com/alvations/nlpnet>`__

Word vectors for named entity recognition and chunking
======================================================

:status: draft
:meant for: undergraduate students

[turian2010word]_ describe a way of improving supervised NLP systems by using word vector representations.

The task
--------

* Replicate their experiments. Check provided `software <http://metaoptimize.com/projects/wordreprs/>`_.
* Experiment with other word vector spaces, for example the spaces used in [EMNLP14]_.

References
==========

.. [CVSC14] Dmitrijs Milajevs and Matthew Purver. `Investigating the Contribution of
  Distributional Semantic Information for Dialogue Act Classification`__. In
  Proceedings of the `EACL 2014`__ Workshop on `Continuous Vector Space Models and
  their Compositionality (CVSC)`__, Gothenburg, April 2014.

  __ {filename}/static/papers/cvsc14/milajevs-purver14cvsc.pdf
  __ http://www.eacl2014.org/
  __ https://sites.google.com/site/cvscworkshop2014/

.. [EMNLP14] Dmitrijs Milajevs, Dimitri Kartsaklis, Mehrnoosh Sadrzadeh and Matthew
  Purver. `Evaluating Neural Word Representations in Tensor-Based Compositional
  Settings`__.   To appear in EMNLP, Doha, Qatar, October 2014.

  __ http://arxiv.org/abs/1408.6179

.. [CoreNLP] Manning, Christopher D., Surdeanu, Mihai, Bauer, John, Finkel, Jenny, Bethard, Steven J., and McClosky,
  David. 2014. `The Stanford CoreNLP Natural Language Processing Toolkit`__. In Proceedings of 52nd Annual Meeting
  of the Association for Computational Linguistics: System Demonstrations, pp. 55-60.

  __ http://nlp.stanford.edu/pubs/StanfordCoreNlp2014.pdf

.. [turian2010word] Turian, Joseph, Lev Ratinov, and Yoshua Bengio. `Word representations: a simple and general method for
  semi-supervised learning`__. Proceedings of the 48th Annual Meeting of the Association for Computational Linguistics.
  Association for Computational Linguistics, 2010.

  __ http://www.aclweb.org/anthology/P/P10/P10-1040.pdf

.. [le2014distributed] Le, Quoc V., and Tomas Mikolov. `Distributed Representations of Sentences and Documents`__.
  arXiv preprint arXiv:1405.4053 (2014).

  __ http://arxiv.org/pdf/1405.4053v2.pdf

.. [blunsom2014morphology] Blunsom, Phil, and Jan Botha. `Compositional Morphology for Word Representations and Language Modelling`__. (2014).

  __ http://arxiv.org/abs/1405.4273

.. [lazaridou2013morphology] Lazaridou, Angeliki, et al. `Compositional-ly Derived Representations of Morphologically Complex Words in Distributional Semantics`__. ACL (1). 2013.

  __ http://www.aclweb.org/anthology/P/P13/P13-1149.pdf
