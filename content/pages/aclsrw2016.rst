========================================================================
Robust Co-occurrence Quantification for Lexical Distributional Semantics
========================================================================

:url: aclsrw2016
:save_as: aclsrw2016/index.html
:date: 2016-05-27 11:56
:slug: aclsrw2016

..

:authors: Dmitrijs Milajevs,  Mehrnoosh Sadrzadeh, Matthew Purver
:venue: `ACL 2016 Student Research Workshop (SRW) <https://sites.google.com/site/aclsrw2016/>`_
:results: `results.csv <{filename}/static/aclsrw2016_results.csv>`_
:supplement: `supplement.zip <{filename}/static/aclsrw2016.zip>`_
:notebook: https://github.com/dimazest/2016-parameters/blob/master/supplement/notebook.ipynb
:repo: https://github.com/dimazest/2016-parameters
