set terminal svg size 600,200 fname 'Helvetica Neue' \
fsize '9' rounded dashed
set output 'olympics_timeline.svg'

set termoption dash

set xdata time
set timefmt "%Y-%m-%d-%H"
set format x "%b-%d"
set format x2 "%b-%d"

set style line 1 lw 1 lt 2 lc rgb "#a0a0a0"
set style line 2 lw 3 lc rgb "#6e56a1"

set grid x

set xlabel "Date"
set ylabel "Number of tweets per hour"

plot \
 "olympics_timeline.csv" using 1:2 w l ls 1 title 'Olympics'

 #"brexit_timeline.csv" using 1:2 w l ls 2smooth bezier title 'Smoothed'
